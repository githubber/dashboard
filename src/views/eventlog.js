'use strict';

/* global angular */
/* global $ */

angular.module('Application').controller('EventLogController', ['$scope', '$location', '$translate', 'Client', function ($scope, $location, $translate, Client) {
    Client.onReady(function () { if (!Client.getUserInfo().isAtLeastAdmin) $location.path('/'); });

    $scope.config = Client.getConfig();

    $scope.busy = false;
    $scope.busyRefresh = false;
    $scope.eventLogs = [];
    $scope.activeEventLog = null;

    // TODO sync this with the eventlog filter
    $scope.actions = [
        { name: '-- All app events --', value: 'app.' },
        { name: '-- All user events --', value: 'user.' },
        { name: 'app.backup', value: 'app.backup' },
        { name: 'app.backup.finish', value: 'app.backup.finish' },
        { name: 'app.configure', value: 'app.configure' },
        { name: 'app.install', value: 'app.install' },
        { name: 'app.restore', value: 'app.restore' },
        { name: 'app.uninstall', value: 'app.uninstall' },
        { name: 'app.update', value: 'app.update' },
        { name: 'app.update.finish', value: 'app.update.finish' },
        { name: 'app.login', value: 'app.login' },
        { name: 'app.oom', value: 'app.oom' },
        { name: 'app.down', value: 'app.down' },
        { name: 'app.up', value: 'app.up' },
        { name: 'app.start', value: 'app.start' },
        { name: 'app.stop', value: 'app.stop' },
        { name: 'app.restart', value: 'app.restart' },
        { name: 'Apptask Crash', value: 'app.task.crash' },
        { name: 'backup.cleanup', value: 'backup.cleanup.start' },
        { name: 'backup.cleanup.finish', value: 'backup.cleanup.finish' },
        { name: 'backup.finish', value: 'backup.finish' },
        { name: 'backup.start', value: 'backup.start' },
        { name: 'certificate.new', value: 'certificate.new' },
        { name: 'certificate.renew', value: 'certificate.renew' },
        { name: 'certificate.cleanup', value: 'certificate.cleanup' },
        { name: 'cloudron.activate', value: 'cloudron.activate' },
        { name: 'cloudron.provision', value: 'cloudron.provision' },
        { name: 'cloudron.restore', value: 'cloudron.restore' },
        { name: 'cloudron.start', value: 'cloudron.start' },
        { name: 'cloudron.update', value: 'cloudron.update' },
        { name: 'cloudron.update.finish', value: 'cloudron.update.finish' },
        { name: 'dashboard.domain.update', value: 'dashboard.domain.update' },
        { name: 'dyndns.update', value: 'dyndns.update' },
        { name: 'domain.add', value: 'domain.add' },
        { name: 'domain.update', value: 'domain.update' },
        { name: 'domain.remove', value: 'domain.remove' },
        { name: 'mail.location', value: 'mail.location' },
        { name: 'mail.enabled', value: 'mail.enabled' },
        { name: 'mail.box.add', value: 'mail.box.add' },
        { name: 'mail.box.update', value: 'mail.box.update' },
        { name: 'mail.box.remove', value: 'mail.box.remove' },
        { name: 'mail.list.add', value: 'mail.list.add' },
        { name: 'mail.list.update', value: 'mail.list.update' },
        { name: 'mail.list.remove', value: 'mail.list.remove' },
        { name: 'service.configure', value: 'service.configure' },
        { name: 'service.rebuild', value: 'service.rebuild' },
        { name: 'service.restart', value: 'service.restart' },
        { name: 'support.ticket', value: 'support.ticket' },
        { name: 'support.ssh', value: 'support.ssh' },
        { name: 'user.add', value: 'user.add' },
        { name: 'user.login', value: 'user.login' },
        { name: 'user.login.ghost', value: 'user.login.ghost' },
        { name: 'user.logout', value: 'user.logout' },
        { name: 'user.remove', value: 'user.remove' },
        { name: 'user.transfer', value: 'user.transfer' },
        { name: 'user.update', value: 'user.update' },
        { name: 'volume.add', value: 'volume.add' },
        { name: 'volume.update', value: 'volume.update' },
        { name: 'volume.remove', value: 'volume.update' },
        { name: 'System Crash', value: 'system.crash' }
    ];

    $scope.pageItemCount = [
        { name: $translate.instant('main.pagination.perPageSelector', { n: 20 }), value: 20 },
        { name: $translate.instant('main.pagination.perPageSelector', { n: 50 }), value: 50 },
        { name: $translate.instant('main.pagination.perPageSelector', { n: 100 }), value: 100 }
    ];

    $scope.currentPage = 1;
    $scope.pageItems = $scope.pageItemCount[0];
    $scope.action = '';
    $scope.selectedActions = [];
    $scope.search = '';

    function fetchEventLogs(background, callback) {
        callback = callback || function (error) { if (error) console.error(error); };
        background = background || false;

        if (!background) $scope.busy = true;

        var actions = $scope.selectedActions.map(function (a) { return a.value; }).join(', ');

        Client.getEventLogs(actions, $scope.search || null, $scope.currentPage, $scope.pageItems.value, function (error, result) {
            $scope.busy = false;

            if (error) return callback(error);

            $scope.eventLogs = [];
            result.forEach(function (e) {
                $scope.eventLogs.push({ raw: e, details: Client.eventLogDetails(e), source: Client.eventLogSource(e) });
            });

            callback();
        });
    }

    $scope.refresh = function () {
        $scope.busyRefresh = true;

        fetchEventLogs(true, function () {
            $scope.busyRefresh = false;
        });
    };

    $scope.showNextPage = function () {
        $scope.currentPage++;
        fetchEventLogs();
    };

    $scope.showPrevPage = function () {
        if ($scope.currentPage > 1) $scope.currentPage--;
        else $scope.currentPage = 1;

        fetchEventLogs();
    };

    $scope.updateFilter = function (fresh) {
        if (fresh) $scope.currentPage = 1;
        fetchEventLogs();
    };

    $scope.showEventLogDetails = function (eventLog) {
        if ($scope.activeEventLog === eventLog) $scope.activeEventLog = null;
        else $scope.activeEventLog = eventLog;
    };

    Client.onReady(function () {
        fetchEventLogs();
    });

    $('.modal-backdrop').remove();
}]);
