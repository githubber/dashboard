'use strict';

/* global $ */
/* global angular */

angular.module('Application').controller('EmailsQueueController', ['$scope', '$location', '$translate', '$timeout', 'Client', function ($scope, $location, $translate, $timeout, Client) {
    Client.onReady(function () { if (!Client.getUserInfo().isAtLeastOwner) $location.path('/'); });

    $scope.ready = false;
    $scope.config = Client.getConfig();
    $scope.user = Client.getUserInfo();

    $scope.pageItemCount = [
        { name: $translate.instant('main.pagination.perPageSelector', { n: 20 }), value: 20 },
        { name: $translate.instant('main.pagination.perPageSelector', { n: 50 }), value: 50 },
        { name: $translate.instant('main.pagination.perPageSelector', { n: 100 }), value: 100 }
    ];

    $scope.queue = {
        busy: true,
        busyRefresh: false,
        items: [],
        activeItem: null,
        currentPage: 1,
        perPage: 20,
        pageItems: $scope.pageItemCount[0],
        search: '',

        refresh: function (showBusy, callback) {
            if (showBusy) $scope.queue.busy = true;

            Client.listMailQueue($scope.queue.search, $scope.queue.currentPage, $scope.queue.pageItems.value, function (error, result) {
                if (showBusy) $scope.queue.busy = false;

                if (error) {
                    console.error('Failed to fetch mail eventlogs.', error);
                } else {
                    $scope.queue.items = result;
                }

                if (callback) callback();
            });
        },

        reload: function () {
            $scope.queue.busyRefresh = true;
            $scope.queue.refresh(true, function () {
                $scope.queue.busyRefresh = false;
            });
        },

        resend: function (item) {
            Client.resendMailQueueItem(item.file, function (error) {
                if (error) return console.error('Failed to retry item.', error);
                $scope.queue.refresh(false);
            });
        },

        discard: function (item) {
            Client.delMailQueueItem(item.file, function (error) {
                if (error) return console.error('Failed to discard item.', error);
                $scope.queue.refresh(false);
            });
        },

        showNextPage: function () {
            $scope.queue.currentPage++;
            $scope.queue.refresh(true);
        },

        showPrevPage: function () {
            if ($scope.queue.currentPage > 1) $scope.queue.currentPage--;
            else $scope.queue.currentPage = 1;
            $scope.queue.refresh(true);
        },

        showItemDetails: function (item) {
            if ($scope.queue.activeItem === item) $scope.queue.activeItem = null;
            else $scope.queue.activeItem = item;
        },

        updateFilter: function (fresh) {
            if (fresh) $scope.queue.currentPage = 1;
            $scope.queue.refresh(false);
        }
    };

    Client.onReady(function () {
        $scope.ready = true;

        $scope.queue.refresh(true);
    });

    $('.modal-backdrop').remove();
}]);
