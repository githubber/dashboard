'use strict';

/* global async */
/* global angular */

angular.module('Application').controller('NotificationsController', ['$scope', '$location', '$timeout', '$translate', '$interval', 'Client', function ($scope, $location, $timeout, $translate, $interval, Client) {
    Client.onReady(function () { if (!Client.getUserInfo().isAtLeastAdmin) $location.path('/'); });

    $scope.clearAllBusy = false;

    $scope.notifications = [];
    $scope.activeNotification = null;
    $scope.busy = true;
    $scope.currentPage = 1;
    $scope.perPage = 20;

    $scope.refresh = function () {
        Client.getNotifications({}, $scope.currentPage, $scope.perPage, function (error, result) {
            if (error) return console.error(error);

            // collapse by default
            result.forEach(function (r) { r.isCollapsed = true; });

            // attempt to translate or parse the message as json
            result.forEach(function (r) {
                try {
                    r.messageJson = JSON.parse(r.message);
                } catch (e) {}
            });

            $scope.notifications = result;

            $scope.busy = false;
        });
    };

    $scope.showNextPage = function () {
        $scope.currentPage++;
        $scope.refresh();
    };

    $scope.showPrevPage = function () {
        if ($scope.currentPage > 1) $scope.currentPage--;
        else $scope.currentPage = 1;

        $scope.refresh();
    };

    $scope.ack = function (notification) {
        if (notification.acknowledged) return;

        Client.ackNotification(notification.id, true, function (error) {
            if (error) console.error(error);

            notification.acknowledged = true;
            $scope.$parent.notificationAcknowledged();
        });
    };

    $scope.clearAll = function () {
        $scope.clearAllBusy = true;

        Client.getNotifications({ acknowledged: false }, 1, 1000, function (error, results) { // hopefully 1k unread is sufficient
            if (error) console.error(error);

            async.eachLimit(results, 20, function (notification, callback) {
                Client.ackNotification(notification.id, true, function (error) {
                    if (error) console.error(error);
                    callback();
                });
            }, function (error) {
                if (error) console.error(error);

                // refresh the main navbar indicator
                $scope.$parent.notificationAcknowledged();
                $scope.refresh();

                $scope.clearAllBusy = false;
            });
        });
    };

    Client.onReady(function () {
        var refreshTimer = $interval($scope.refresh, 60 * 1000); // keep this interval in sync with the notification count indicator in main.js
        $scope.$on('$destroy', function () {
            $interval.cancel(refreshTimer);
        });
        $scope.refresh();
    });
}]);
